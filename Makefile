LATEXOPTS=-shell-escape -interaction nonstopmode

.PHONY: clean all todos

all: skript.pdf todos

skript.pdf: skript.tex
		pdflatex ${LATEXOPTS} skript.tex
		pdflatex ${LATEXOPTS} skript.tex	
		pdflatex ${LATEXOPTS} skript.tex
		makeindex -s gatech-thesis-index skript.idx
		pdflatex ${LATEXOPTS} skript.tex
		pdflatex ${LATEXOPTS} skript.tex	
		pdflatex ${LATEXOPTS} skript.tex

todos:
	@echo
	@echo "************************************************************"
	@echo "Das Mathe-Skript ist (wahrscheinlich) noch nicht perfekt."
	@echo "Hier sind einige Stellen, an denen noch Hilfe benoetigt wird:"
	@echo
	@grep -En --color=auto '(TODO|FIXME)' skript.tex

clean:
	rm skript.pdf skript.aux skript.toc skript.log skript.ind skript.ilg skript.idx skript.out
